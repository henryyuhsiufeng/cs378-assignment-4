// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "CS378_Assignment_4Character.generated.h"

class UTextRenderComponent;

/**
 * This class is the default character for CS378_Assignment_4, and it is responsible for all
 * physical interaction between the player and the world.
 *
 * The capsule component (inherited from ACharacter) handles collision with the world
 * The CharacterMovementComponent (inherited from ACharacter) handles movement of the collision capsule
 * The Sprite component (inherited from APaperCharacter) handles the visuals
 */
UCLASS(config=Game)
class ACS378_Assignment_4Character : public APaperCharacter
{
	GENERATED_BODY()

	/** Side view camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera, meta=(AllowPrivateAccess="true"))
	class UCameraComponent* SideViewCameraComponent;

	/** Camera boom positioning the camera beside the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	UTextRenderComponent* TextComponent;
	virtual void Tick(float DeltaSeconds) override;

protected:

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// The animation to play while running around
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Animations)
	class UPaperFlipbook* RunningAnimation;

	// The animation to play while idle (standing still)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
	class UPaperFlipbook* IdleAnimation;

	/** Called to choose the correct animation to play based on the character's movement state */
	void UpdateAnimation();

	/** Called for side to side input */
	void MoveRight(float Value);

	void UpdateCharacter();

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);


	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

public:
	ACS378_Assignment_4Character();

	UPROPERTY(VisibleDefaultsOnly)
		class USceneComponent* WeaponLocation;

	UPROPERTY(BlueprintReadWrite)
		class AWeaponBase* WeaponInstance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = GamePlay)
		float MaxHealth;

	float RemainingHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AWeaponBase> MainWeaponClass;

	UFUNCTION(BlueprintCallable)
		void FireWeapon(float Value);

	UFUNCTION(BlueprintCallable)
		virtual void UpdateHealth(float Change);

	UFUNCTION()
		void TakeAnyDamage(AActor* DamagedActor, float inDamage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	/** Returns SideViewCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetSideViewCameraComponent() const { return SideViewCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
};
