// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class CS378_Assignment_4 : ModuleRules
{
	public CS378_Assignment_4(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "Paper2D" });
	}
}
