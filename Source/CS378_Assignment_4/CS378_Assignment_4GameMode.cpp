// Copyright Epic Games, Inc. All Rights Reserved.

#include "CS378_Assignment_4GameMode.h"
#include "CS378_Assignment_4Character.h"

ACS378_Assignment_4GameMode::ACS378_Assignment_4GameMode()
{
	// Set default pawn class to our character
	DefaultPawnClass = ACS378_Assignment_4Character::StaticClass();	
}
