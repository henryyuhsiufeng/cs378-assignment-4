// Fill out your copyright notice in the Description page of Project Settings.

#include "ProjectileBase.h"
#include "Engine/EngineTypes.h"
#include "WeaponBase.h"

// Sets default values
AWeaponBase::AWeaponBase()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon Mesh"));
	StaticMeshComponent->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	StaticMeshComponent->SetGenerateOverlapEvents(false);
	RootComponent = StaticMeshComponent;

	MainProjectileClass = AProjectileBase::StaticClass();
	FireRate = 1.0f;
	bIsFiring = false;
}

// Called when the game starts or when spawned
void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWeaponBase::FireProjectile(float Value)
{
	if (Value && !bIsFiring)
	{
		bIsFiring = true;
		GetWorldTimerManager().SetTimer(FiringTimer, this, &AWeaponBase::HandleFireTimer, FireRate, false);
		FActorSpawnParameters SpawnParameters;
		SpawnParameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;
		SpawnParameters.Instigator = GetInstigator();
		SpawnParameters.Owner = this;
		GetWorld()->SpawnActor<AProjectileBase>(MainProjectileClass, GetActorLocation(), GetActorRotation(), SpawnParameters);
	}
}

void AWeaponBase::HandleFireTimer()
{
	bIsFiring = false;
}
