// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PaperCharacter.h"
#include "Components/SphereComponent.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "Engine/CollisionProfile.h"
#include "EnemyBase.generated.h"

class UTextRenderComponent;

/**
 * This class is the default character for CS378_Assignment_4, and it is responsible for all
 * physical interaction between the player and the world.
 *
 * The capsule component (inherited from ACharacter) handles collision with the world
 * The CharacterMovementComponent (inherited from ACharacter) handles movement of the collision capsule
 * The Sprite component (inherited from APaperCharacter) handles the visuals
 */
UCLASS(config = Game)
class CS378_ASSIGNMENT_4_API AEnemyBase : public APaperCharacter
{
	GENERATED_BODY()

	UTextRenderComponent* TextComponent;
	virtual void Tick(float DeltaSeconds) override;
protected:
	// The animation to play while running around
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
		class UPaperFlipbook* RunningAnimation;

	// The animation to play while idle (standing still)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Animations)
		class UPaperFlipbook* IdleAnimation;

	/** Called to choose the correct animation to play based on the character's movement state */
	void UpdateAnimation();

	/** Called for side to side input */
	void MoveRight(float Value);

	void UpdateCharacter();

	/** Handle touch inputs. */
	void TouchStarted(const ETouchIndex::Type FingerIndex, const FVector Location);

	/** Handle touch stop event. */
	void TouchStopped(const ETouchIndex::Type FingerIndex, const FVector Location);

	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
	// End of APawn interface

	UPROPERTY(VisibleDefaultsOnly)
		class USceneComponent* WeaponLocation;

	UPROPERTY(BlueprintReadWrite)
		class AWeaponBase* WeaponInstance;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = GamePlay)
		float MaxHealth;

	float RemainingHealth;

	UFUNCTION()
		void TakeAnyDamage(AActor* DamagedActor, float inDamage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

public:
	AEnemyBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class AWeaponBase> MainWeaponClass;

	UFUNCTION(BlueprintCallable)
		void FireWeapon(float Value);

	UFUNCTION(BlueprintCallable)
		virtual void UpdateHealth(float Change);

};
