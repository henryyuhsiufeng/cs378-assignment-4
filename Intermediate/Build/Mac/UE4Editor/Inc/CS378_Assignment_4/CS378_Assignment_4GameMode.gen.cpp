// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "CS378_Assignment_4/CS378_Assignment_4GameMode.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeCS378_Assignment_4GameMode() {}
// Cross Module References
	CS378_ASSIGNMENT_4_API UClass* Z_Construct_UClass_ACS378_Assignment_4GameMode_NoRegister();
	CS378_ASSIGNMENT_4_API UClass* Z_Construct_UClass_ACS378_Assignment_4GameMode();
	ENGINE_API UClass* Z_Construct_UClass_AGameModeBase();
	UPackage* Z_Construct_UPackage__Script_CS378_Assignment_4();
// End Cross Module References
	void ACS378_Assignment_4GameMode::StaticRegisterNativesACS378_Assignment_4GameMode()
	{
	}
	UClass* Z_Construct_UClass_ACS378_Assignment_4GameMode_NoRegister()
	{
		return ACS378_Assignment_4GameMode::StaticClass();
	}
	struct Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AGameModeBase,
		(UObject* (*)())Z_Construct_UPackage__Script_CS378_Assignment_4,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * The GameMode defines the game being played. It governs the game rules, scoring, what actors\n * are allowed to exist in this game type, and who may enter the game.\n *\n * This game mode just sets the default pawn to be the MyCharacter asset, which is a subclass of CS378_Assignment_4Character\n */" },
		{ "HideCategories", "Info Rendering MovementReplication Replication Actor Input Movement Collision Rendering Utilities|Transformation" },
		{ "IncludePath", "CS378_Assignment_4GameMode.h" },
		{ "ModuleRelativePath", "CS378_Assignment_4GameMode.h" },
		{ "ShowCategories", "Input|MouseInput Input|TouchInput" },
		{ "ToolTip", "The GameMode defines the game being played. It governs the game rules, scoring, what actors\nare allowed to exist in this game type, and who may enter the game.\n\nThis game mode just sets the default pawn to be the MyCharacter asset, which is a subclass of CS378_Assignment_4Character" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ACS378_Assignment_4GameMode>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics::ClassParams = {
		&ACS378_Assignment_4GameMode::StaticClass,
		"Game",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x008802ACu,
		METADATA_PARAMS(Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ACS378_Assignment_4GameMode()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ACS378_Assignment_4GameMode, 1409415127);
	template<> CS378_ASSIGNMENT_4_API UClass* StaticClass<ACS378_Assignment_4GameMode>()
	{
		return ACS378_Assignment_4GameMode::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ACS378_Assignment_4GameMode(Z_Construct_UClass_ACS378_Assignment_4GameMode, &ACS378_Assignment_4GameMode::StaticClass, TEXT("/Script/CS378_Assignment_4"), TEXT("ACS378_Assignment_4GameMode"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ACS378_Assignment_4GameMode);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
