// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UDamageType;
class AController;
#ifdef CS378_ASSIGNMENT_4_EnemyBase_generated_h
#error "EnemyBase.generated.h already included, missing '#pragma once' in EnemyBase.h"
#endif
#define CS378_ASSIGNMENT_4_EnemyBase_generated_h

#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_SPARSE_DATA
#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execUpdateHealth); \
	DECLARE_FUNCTION(execFireWeapon); \
	DECLARE_FUNCTION(execTakeAnyDamage);


#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execUpdateHealth); \
	DECLARE_FUNCTION(execFireWeapon); \
	DECLARE_FUNCTION(execTakeAnyDamage);


#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAEnemyBase(); \
	friend struct Z_Construct_UClass_AEnemyBase_Statics; \
public: \
	DECLARE_CLASS(AEnemyBase, APaperCharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), NO_API) \
	DECLARE_SERIALIZER(AEnemyBase)


#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_INCLASS \
private: \
	static void StaticRegisterNativesAEnemyBase(); \
	friend struct Z_Construct_UClass_AEnemyBase_Statics; \
public: \
	DECLARE_CLASS(AEnemyBase, APaperCharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), NO_API) \
	DECLARE_SERIALIZER(AEnemyBase)


#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AEnemyBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AEnemyBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyBase(AEnemyBase&&); \
	NO_API AEnemyBase(const AEnemyBase&); \
public:


#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AEnemyBase(AEnemyBase&&); \
	NO_API AEnemyBase(const AEnemyBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AEnemyBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AEnemyBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AEnemyBase)


#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__RunningAnimation() { return STRUCT_OFFSET(AEnemyBase, RunningAnimation); } \
	FORCEINLINE static uint32 __PPO__IdleAnimation() { return STRUCT_OFFSET(AEnemyBase, IdleAnimation); } \
	FORCEINLINE static uint32 __PPO__WeaponLocation() { return STRUCT_OFFSET(AEnemyBase, WeaponLocation); } \
	FORCEINLINE static uint32 __PPO__WeaponInstance() { return STRUCT_OFFSET(AEnemyBase, WeaponInstance); } \
	FORCEINLINE static uint32 __PPO__MaxHealth() { return STRUCT_OFFSET(AEnemyBase, MaxHealth); }


#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_22_PROLOG
#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_RPC_WRAPPERS \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_INCLASS \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_RPC_WRAPPERS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_INCLASS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h_25_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_ASSIGNMENT_4_API UClass* StaticClass<class AEnemyBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID cs378_assignment_4_Source_CS378_Assignment_4_EnemyBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
