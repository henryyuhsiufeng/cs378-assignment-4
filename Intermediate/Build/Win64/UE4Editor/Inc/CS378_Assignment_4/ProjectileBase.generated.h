// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class UPrimitiveComponent;
class AActor;
struct FHitResult;
#ifdef CS378_ASSIGNMENT_4_ProjectileBase_generated_h
#error "ProjectileBase.generated.h already included, missing '#pragma once' in ProjectileBase.h"
#endif
#define CS378_ASSIGNMENT_4_ProjectileBase_generated_h

#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_SPARSE_DATA
#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execBeginOverlap);


#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execBeginOverlap);


#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAProjectileBase(); \
	friend struct Z_Construct_UClass_AProjectileBase_Statics; \
public: \
	DECLARE_CLASS(AProjectileBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), NO_API) \
	DECLARE_SERIALIZER(AProjectileBase)


#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAProjectileBase(); \
	friend struct Z_Construct_UClass_AProjectileBase_Statics; \
public: \
	DECLARE_CLASS(AProjectileBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), NO_API) \
	DECLARE_SERIALIZER(AProjectileBase)


#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AProjectileBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AProjectileBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectileBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectileBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectileBase(AProjectileBase&&); \
	NO_API AProjectileBase(const AProjectileBase&); \
public:


#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AProjectileBase(AProjectileBase&&); \
	NO_API AProjectileBase(const AProjectileBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AProjectileBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AProjectileBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AProjectileBase)


#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__CollisionComponent() { return STRUCT_OFFSET(AProjectileBase, CollisionComponent); } \
	FORCEINLINE static uint32 __PPO__MeshComponent() { return STRUCT_OFFSET(AProjectileBase, MeshComponent); } \
	FORCEINLINE static uint32 __PPO__ProjectileMovementComponent() { return STRUCT_OFFSET(AProjectileBase, ProjectileMovementComponent); } \
	FORCEINLINE static uint32 __PPO__Damage() { return STRUCT_OFFSET(AProjectileBase, Damage); }


#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_9_PROLOG
#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_RPC_WRAPPERS \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_INCLASS \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_INCLASS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_ASSIGNMENT_4_API UClass* StaticClass<class AProjectileBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID cs378_assignment_4_Source_CS378_Assignment_4_ProjectileBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
