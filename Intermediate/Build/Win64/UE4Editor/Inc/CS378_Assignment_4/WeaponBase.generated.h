// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CS378_ASSIGNMENT_4_WeaponBase_generated_h
#error "WeaponBase.generated.h already included, missing '#pragma once' in WeaponBase.h"
#endif
#define CS378_ASSIGNMENT_4_WeaponBase_generated_h

#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_SPARSE_DATA
#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execFireProjectile); \
	DECLARE_FUNCTION(execHandleFireTimer);


#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execFireProjectile); \
	DECLARE_FUNCTION(execHandleFireTimer);


#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAWeaponBase(); \
	friend struct Z_Construct_UClass_AWeaponBase_Statics; \
public: \
	DECLARE_CLASS(AWeaponBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), NO_API) \
	DECLARE_SERIALIZER(AWeaponBase)


#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_INCLASS \
private: \
	static void StaticRegisterNativesAWeaponBase(); \
	friend struct Z_Construct_UClass_AWeaponBase_Statics; \
public: \
	DECLARE_CLASS(AWeaponBase, AActor, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), NO_API) \
	DECLARE_SERIALIZER(AWeaponBase)


#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AWeaponBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AWeaponBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponBase(AWeaponBase&&); \
	NO_API AWeaponBase(const AWeaponBase&); \
public:


#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AWeaponBase(AWeaponBase&&); \
	NO_API AWeaponBase(const AWeaponBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AWeaponBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AWeaponBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AWeaponBase)


#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__StaticMeshComponent() { return STRUCT_OFFSET(AWeaponBase, StaticMeshComponent); }


#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_9_PROLOG
#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_RPC_WRAPPERS \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_INCLASS \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_RPC_WRAPPERS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_INCLASS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h_12_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_ASSIGNMENT_4_API UClass* StaticClass<class AWeaponBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID cs378_assignment_4_Source_CS378_Assignment_4_WeaponBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
