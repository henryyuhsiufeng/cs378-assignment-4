// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
class AActor;
class UDamageType;
class AController;
#ifdef CS378_ASSIGNMENT_4_CS378_Assignment_4Character_generated_h
#error "CS378_Assignment_4Character.generated.h already included, missing '#pragma once' in CS378_Assignment_4Character.h"
#endif
#define CS378_ASSIGNMENT_4_CS378_Assignment_4Character_generated_h

#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_SPARSE_DATA
#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execTakeAnyDamage); \
	DECLARE_FUNCTION(execUpdateHealth); \
	DECLARE_FUNCTION(execFireWeapon);


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execTakeAnyDamage); \
	DECLARE_FUNCTION(execUpdateHealth); \
	DECLARE_FUNCTION(execFireWeapon);


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACS378_Assignment_4Character(); \
	friend struct Z_Construct_UClass_ACS378_Assignment_4Character_Statics; \
public: \
	DECLARE_CLASS(ACS378_Assignment_4Character, APaperCharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), NO_API) \
	DECLARE_SERIALIZER(ACS378_Assignment_4Character)


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_INCLASS \
private: \
	static void StaticRegisterNativesACS378_Assignment_4Character(); \
	friend struct Z_Construct_UClass_ACS378_Assignment_4Character_Statics; \
public: \
	DECLARE_CLASS(ACS378_Assignment_4Character, APaperCharacter, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), NO_API) \
	DECLARE_SERIALIZER(ACS378_Assignment_4Character)


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ACS378_Assignment_4Character(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACS378_Assignment_4Character) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACS378_Assignment_4Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Assignment_4Character); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACS378_Assignment_4Character(ACS378_Assignment_4Character&&); \
	NO_API ACS378_Assignment_4Character(const ACS378_Assignment_4Character&); \
public:


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ACS378_Assignment_4Character(ACS378_Assignment_4Character&&); \
	NO_API ACS378_Assignment_4Character(const ACS378_Assignment_4Character&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ACS378_Assignment_4Character); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Assignment_4Character); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACS378_Assignment_4Character)


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_PRIVATE_PROPERTY_OFFSET \
	FORCEINLINE static uint32 __PPO__SideViewCameraComponent() { return STRUCT_OFFSET(ACS378_Assignment_4Character, SideViewCameraComponent); } \
	FORCEINLINE static uint32 __PPO__CameraBoom() { return STRUCT_OFFSET(ACS378_Assignment_4Character, CameraBoom); } \
	FORCEINLINE static uint32 __PPO__RunningAnimation() { return STRUCT_OFFSET(ACS378_Assignment_4Character, RunningAnimation); } \
	FORCEINLINE static uint32 __PPO__IdleAnimation() { return STRUCT_OFFSET(ACS378_Assignment_4Character, IdleAnimation); }


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_19_PROLOG
#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_RPC_WRAPPERS \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_INCLASS \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_RPC_WRAPPERS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_INCLASS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h_22_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_ASSIGNMENT_4_API UClass* StaticClass<class ACS378_Assignment_4Character>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4Character_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
