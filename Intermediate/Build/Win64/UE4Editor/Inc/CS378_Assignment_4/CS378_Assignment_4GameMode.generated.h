// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef CS378_ASSIGNMENT_4_CS378_Assignment_4GameMode_generated_h
#error "CS378_Assignment_4GameMode.generated.h already included, missing '#pragma once' in CS378_Assignment_4GameMode.h"
#endif
#define CS378_ASSIGNMENT_4_CS378_Assignment_4GameMode_generated_h

#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_SPARSE_DATA
#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_RPC_WRAPPERS
#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_RPC_WRAPPERS_NO_PURE_DECLS
#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesACS378_Assignment_4GameMode(); \
	friend struct Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics; \
public: \
	DECLARE_CLASS(ACS378_Assignment_4GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), CS378_ASSIGNMENT_4_API) \
	DECLARE_SERIALIZER(ACS378_Assignment_4GameMode)


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_INCLASS \
private: \
	static void StaticRegisterNativesACS378_Assignment_4GameMode(); \
	friend struct Z_Construct_UClass_ACS378_Assignment_4GameMode_Statics; \
public: \
	DECLARE_CLASS(ACS378_Assignment_4GameMode, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient | CLASS_Config), CASTCLASS_None, TEXT("/Script/CS378_Assignment_4"), CS378_ASSIGNMENT_4_API) \
	DECLARE_SERIALIZER(ACS378_Assignment_4GameMode)


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	CS378_ASSIGNMENT_4_API ACS378_Assignment_4GameMode(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ACS378_Assignment_4GameMode) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CS378_ASSIGNMENT_4_API, ACS378_Assignment_4GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Assignment_4GameMode); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CS378_ASSIGNMENT_4_API ACS378_Assignment_4GameMode(ACS378_Assignment_4GameMode&&); \
	CS378_ASSIGNMENT_4_API ACS378_Assignment_4GameMode(const ACS378_Assignment_4GameMode&); \
public:


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	CS378_ASSIGNMENT_4_API ACS378_Assignment_4GameMode(ACS378_Assignment_4GameMode&&); \
	CS378_ASSIGNMENT_4_API ACS378_Assignment_4GameMode(const ACS378_Assignment_4GameMode&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(CS378_ASSIGNMENT_4_API, ACS378_Assignment_4GameMode); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ACS378_Assignment_4GameMode); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(ACS378_Assignment_4GameMode)


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_PRIVATE_PROPERTY_OFFSET
#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_15_PROLOG
#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_RPC_WRAPPERS \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_INCLASS \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_PRIVATE_PROPERTY_OFFSET \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_SPARSE_DATA \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_RPC_WRAPPERS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_INCLASS_NO_PURE_DECLS \
	cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h_18_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> CS378_ASSIGNMENT_4_API UClass* StaticClass<class ACS378_Assignment_4GameMode>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID cs378_assignment_4_Source_CS378_Assignment_4_CS378_Assignment_4GameMode_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
