The tileset: tileset.png

    The original size of each tile is 32 * 32

    Each tile has multiple textures, most are generated from a 2 * 2 subset of tiles
    	The subsets are usually 64 * 64 pixels
    	Take the brick for example, there are 4 textures which can be reandomly selected for a brick tile

    The lighter variations are platform textures, so they affect the physics of the game
    The darker variations are wall textures, so only use them as backgrounds

    The tileset.png has been scaled up by a factor of 4 to make the art visible on screen.
    	This means that the size of each tile is now 128 * 128, take this into account when splitting up the tiles in the game

The characters: characters.png

    The orignal size of each tile is 32 * 64

    The chracters are as follows from left to right:

    	Intern:
    		A Junior from some University
    		CS major
    		leetcoded like crazy to get into Saharah
    		100 health
    		No Projectiles
    		goes back and forth on platforms / when it hits a wall

    	Entry:
    		This is his first job
    		Also probably his last
    		Hair loss from stress
    		bloodshot eyes from staring at a screen too long
    		200 health
    		No Projectiles
    		goes back and forth on platforms / when it hits a wall
    	Junior:
    		Works from home now, more specifically his moms basement
    		Thinks in code
    		cloud developer who thinks he's a hacker
    		150 health
    		Shoots DDOS
    		goes back and forth on platforms / when it hits a wall
    	Senior:
    		Asked for a raise, trancended humanity
    		Works unpaid overtime
    		When she gets paid, she gets paid in eyedrops
    		300 health
    		Shoots everything st random
    		pathfinds to Don
    	Don:
    		The protagonist (playable character)
    		WASD to move, spacebar to shoot
    		aim projectiles with mouse
    		Can get hit by his own projectiles if not careful
    		use 1, 2 ,3 keys to change weapons
    		1 - DDOS - Normal Attack, spam until developer can’t code anymore. Like a minigun. Minus 5-30 health each hit
    		2 - Trojan - Delayed explosive package, programmer picks up, and explodes. Explosion insta kills developer and does 50-100 damage to any nearby developers including Don
    		3 - Worm - Like a sniper. Turn the developer you hit into your ally. If they come in contact with other developers, they infect them. Infected die after 20 seconds.

    The characters.png has been scaled up by a factor of 4 to make the art visible on screen.
    	This means that the size of each character is now 128 * 256, take this into account when splitting up the character sprites in the game

The projectiles: projectiles.png

    The orginal size of each projectile is 8 * 8

    The projectiles are as follows from left to right:
    	DDOS - DDOS has two sprites which generate randomly to simulate a binary stream
    		'0'
    		'1'
    	Trojan - The bomb looking one
    	Worm - THe one with an image similar to the python logo

    There is no difference between enemy projectiles and your projectiles, so everyone can be hit by everything. Be careful

    The projectiles.png has been scaled up by a factor of 4 to make the art visible on screen.
    	This means that the size of each projectile is now 32 * 32, take this into account when splitting up the projectile sprites in the game

The background: background_1.png

    This is the background for the first level

    The backgrounds been scaled up by a factor of 4 to make the art visible on screen.

The boss: drone.png
The original size of the drone is 64 \* 64
The drone has really strong antivirus, so only DDOS works on the drone
Flies around and breaks tiles
If hits Don, Don takes 5-20 damage

    The drone.png has been scaled up by a factor of 4 to make art visible on screen. The size of the drone in game is 256 * 256

###### Links

Game Demo:

- [![Game Demo](https://i9.ytimg.com/vi_webp/QElAiPXeeQk/mqdefault.webp?time=1607530800000&sqp=CLDyw_4F&rs=AOn4CLBo0YXBC040C-J2qE010qW1_vBnMg)](https://youtu.be/QElAiPXeeQk "Game Demo - Click Me!")

Game Trailer:

- https://drive.google.com/file/d/1ygAaUvxT2ccA8Ku15ucdcF-LMRJHwmiM/view?usp=sharing

Game Clips:

- [Clip 1](https://drive.google.com/file/d/1-mFU17Dn2tw5cYtdrsp9G57iQRWL4Ekc/view)
- [Clip 2](https://drive.google.com/drive/folders/1Laldk-dyl9Oef-JuctBpNAnfFi4SQBWb?usp=sharing)
- [Clip 3](https://drive.google.com/file/d/1ihR0iDQIAqFtkASyajpGjBH4Cka41SrV/view)
